## Instalação do GO Lang:

Executar a instalação do GO através do site oficial:
https://golang.org/dl/

Mover a pasta crud-go-backend para o root do go, no meu caso o diretório é:

C:\Users\SeuUsuario\go\src\github.com\

## Criação do BD e tabelas:

Executar no banco mysql o arquivo SCRIPT_BANCO.sql


## Execução do backend:

na pasta C:\Users\SeuUsuario\go\src\github.com\crud-go-backend, execute o comando:

`go build`

e depois execute o arquivo gerado:

`./crud-go-backend`


Rotas:

Método: POST (http://localhost:8080/api/v1/funcionarios
Parâmetro: DataCad: Date, Nome: string,UFNasc: string, Salario: string, Status: string
Retorna: Array de objetos do tipo Funcionário.


Método PUT (http://localhost:8080/api/v1/funcionarios/{id}
Parâmetro: Id do funcionario
Body: DataCad: Date, Nome: string,UFNasc: string, Salario: string, Status: string
Retorno: Objeto funcionário alterado.

Método GET (http://localhost:8080/api/v1/funcionarios
Parâmetros de filtro opcionais: DataCad: Date, Nome: string, UfNasc: string, SalarioMin: number, SalarioMax: number, Status: string
Retorno: Objeto funcionário filtrado

Método GET (http://localhost:8080/api/v1/funcionarios/{id}
Parâmetro: Id do funcionário
Retorno: Objeto funcionario por id.

Método DELETE (http://localhost:8080/api/v1/funcionarios{cpf}
Parâmetro: Cpf do funcionário
Retorno: Objeto funcionario excluido do banco.
