package api

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/crud-go-backend/api/handler"
	"github.com/crud-go-backend/gorilla/mux"
)

// App struct ...
type App struct {
	Router *mux.Router
	DB     *sql.DB
}

//StartServer ...
func (a *App) StartServer() {
	a.Router = mux.NewRouter()
	s := a.Router.PathPrefix("/api/v1").Subrouter()

	//Funcionarios
	s.HandleFunc("/funcionarios", handler.InsertFuncionario).Methods(http.MethodPost)
	s.HandleFunc("/funcionarios/{id:[0-9]+}", handler.UpdateFuncionario).Methods(http.MethodPut)
	s.HandleFunc("/funcionarios/{id:[0-9]+}", handler.DeleteFuncionario).Methods(http.MethodDelete)
	s.HandleFunc("/funcionarios/{id:[0-9]+}", handler.GetFuncionario).Methods(http.MethodGet)
	s.HandleFunc("/funcionarios", handler.GetFuncionarios).Methods(http.MethodGet)

	a.Router.Handle("/api/v1/{_:.*}", a.Router)
	port := 8080

	log.Printf("Starting Server on port %d", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), a.Router))
}
