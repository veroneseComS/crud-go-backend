package handler

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/crud-go-backend/api/db"
	"github.com/crud-go-backend/model"
	"github.com/crud-go-backend/util"

	"github.com/crud-go-backend/gorilla/mux"
)

//InsertFuncionario
func InsertFuncionario(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	var t util.App
	var f model.Funcionario
	var d db.DB
	err := d.Connection()
	if err != nil {
		t.ResponseWithError(w, http.StatusInternalServerError, "Não foi possível conectar ao BD", "")
		return
	}
	db := d.DB
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&f); err != nil {
		t.ResponseWithError(w, http.StatusBadRequest, "Invalid request payload", err.Error())
		return
	}
	defer r.Body.Close()
	err = f.InsertFuncionario(db)
	if err != nil {
		t.ResponseWithError(w, http.StatusBadRequest, "Erro ao inserir funcionário", "")
		return
	}
	t.ResponseWithJSON(w, http.StatusOK, f, 0, 0)
}

//UpdateFuncionario
func UpdateFuncionario(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	var f model.Funcionario
	var t util.App
	var d db.DB
	err := d.Connection()
	if err != nil {
		log.Printf("[handler/UpdateFuncionario] -  Erro ao tentar abrir conexão. Erro: %s", err.Error())
		return
	}
	db := d.DB
	defer db.Close()

	vars := mux.Vars(r)
	Id, err := strconv.Atoi(vars["Id"])
	if err != nil {
		t.ResponseWithError(w, http.StatusBadRequest, "Id inválido", "")
		return
	}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&f); err != nil {
		t.ResponseWithError(w, http.StatusBadRequest, "Invalid request payload", "")
		return
	}
	defer r.Body.Close()
	f.Id = int64(Id)
	if err := f.UpdateFuncionario(db); err != nil {
		t.ResponseWithError(w, http.StatusInternalServerError, err.Error(), "")
		return
	}
	t.ResponseWithJSON(w, http.StatusOK, f, 0, 0)
}

//DeleteFuncionario
func DeleteFuncionario(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	var f model.Funcionario
	var d db.DB
	var t util.App
	err := d.Connection()
	if err != nil {
		log.Printf("[handler/DeleteFuncionario -  Erro ao tentar abrir conexão. Erro: %s", err.Error())
		return
	}
	db := d.DB
	defer db.Close()

	vars := mux.Vars(r)
	Cpf, err := strconv.Atoi(vars["Cpf"])
	if err != nil {
		t.ResponseWithError(w, http.StatusBadRequest, "CPF Inválido", "")
		return
	}

	f.Cpf = string(Cpf)
	if err := f.DeleteFuncionario(db); err != nil {
		log.Printf("[handler/DeleteFuncionario -  Erro ao tentar deletar funcionário. Erro: %s", err.Error())
		t.ResponseWithError(w, http.StatusInternalServerError, err.Error(), "")
		return
	}
	t.ResponseWithJSON(w, http.StatusOK, f, 0, 0)
}

//GetFuncionario
func GetFuncionario(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	var f model.Funcionario
	var t util.App
	var d db.DB
	err := d.Connection()
	if err != nil {
		log.Printf("[handler/GetFuncionario] -  Erro ao tentar abrir conexão. Erro: %s", err.Error())
		return
	}
	db := d.DB
	defer db.Close()

	vars := mux.Vars(r)
	Id, err := strconv.Atoi(vars["Id"])
	if err != nil {
		t.ResponseWithError(w, http.StatusBadRequest, "Id inválido", "")
		return
	}

	f.Id = int64(Id)
	err = f.GetFuncionario(db)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Printf("[handler/GetFuncionario -  Não há funcionário com este ID.")
			t.ResponseWithError(w, http.StatusInternalServerError, "Não há funcionário com este ID.", err.Error())
		} else {
			log.Printf("[handler/GetFuncionario -  Erro ao tentar buscar funcionário. Erro: %s", err.Error())
			t.ResponseWithError(w, http.StatusInternalServerError, err.Error(), "")
		}
		return
	}
	t.ResponseWithJSON(w, http.StatusOK, f, 0, 0)
}

//GetFuncionarios
func GetFuncionarios(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	var f model.Funcionario
	var t util.App
	var d db.DB
	err := d.Connection()
	if err != nil {
		log.Printf("[handler/GetFuncionarios] -  Erro ao tentar abrir conexão. Erro: %s", err.Error())
		return
	}
	db := d.DB
	defer db.Close()

	Id, _ := strconv.Atoi(r.FormValue("Id"))
	DataCad := r.FormValue("DataCad")
	Nome := r.FormValue("Nome")
	Cpf := r.FormValue("Cpf")
	Cargo := r.FormValue("Cargo")
	SalarioMin, err := strconv.ParseFloat(r.FormValue("SalarioMin"), 64)
	SalarioMax, err := strconv.ParseFloat(r.FormValue("SalarioMax"), 64)

	f.Id = int64(Id)
	f.DataCad = DataCad
	f.Nome = Nome
	f.Cpf = Cpf
	f.Cargo = Cargo
	f.SalarioMin = float64(SalarioMin)
	f.SalarioMax = float64(SalarioMax)

	funcionarios, err := f.GetFuncionarios(db)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Printf("[handler/GetFuncionarios -  Não há funcionário com este ID.")
			t.ResponseWithError(w, http.StatusInternalServerError, "Não há funcionários cadastrados.", err.Error())
		} else {
			log.Printf("[handler/GetFuncionarios -  Erro ao tentar buscar funcionários. Erro: %s", err.Error())
			t.ResponseWithError(w, http.StatusInternalServerError, err.Error(), "")
		}
		return
	}
	t.ResponseWithJSON(w, http.StatusOK, funcionarios, 0, 0)
}
