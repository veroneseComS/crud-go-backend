package main

import (
	"log"

	"github.com/crud-go-backend/api"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile) //Informa o local do erro
	api := api.App{}
	api.StartServer()
}
