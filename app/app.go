package app

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/crud-go-backend/db"
	"github.com/crud-go-backend/gorilla/mux"
)

type Funcionario struct {
	Id      int     `json:"Id"`
	DataCad string  `json:"DataCad"`
	Cargo   string  `json:"Cargo"`
	Cpf     string  `json:"Cpf"`
	Nome    string  `json:"Nome"`
	UfNasc  string  `json:"UfNasc"`
	Salario float64 `json:"Salario"`
	Status  string  `json:"Status"`
}

type App struct {
	Router   *mux.Router
	Database *sql.DB
}

func (app *App) SetupRouter() {
	app.Router.
		Methods("POST").
		Path("/api/funcionarios").
		HandlerFunc(app.createFuncionarios)

	app.Router.
		Methods("GET").
		Path("/api/funcionarios").
		HandlerFunc(app.getFuncionarios)
}

//POST Funcionarios
func (app *App) createFuncionarios(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)

	var data Funcionario
	err := decoder.Decode(&data)
	if err != nil {
		panic(err)
	}

	database, err := db.CreateDatabase()
	if err != nil {
		log.Fatal("Não foi possível se conectar no banco de dados")
	}
	_, err = database.Exec(`INSERT INTO funcionarios (DataCad, Cargo, Cpf, Nome, UfNasc, Salario, Status) VALUES (?, ?, ?, ?, ?, ?, ?)`, data.DataCad, data.Cargo, data.Cpf, data.Nome, data.UfNasc, data.Salario, data.Status)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Funcionário inserido com sucesso.")
	w.WriteHeader(http.StatusOK)
}

//GET Funcionarios
func (app *App) getFuncionarios(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	Nome := v.Get("Nome")
	Cpf := v.Get("Cpf")

	log.Println(v)

	funcionarios := make([]*Funcionario, 0)

	result, err := app.Database.Query("SELECT * FROM `funcionarios`")

	if err != nil {
		log.Println(err)
	}

	defer result.Close()

	for result.Next() {

		var possuiFiltro bool = false

		funcionario := new(Funcionario)
		if err := result.Scan(&funcionario.Id, &funcionario.DataCad, &funcionario.Cargo, &funcionario.Cpf, &funcionario.Nome, &funcionario.UfNasc, &funcionario.Salario, &funcionario.Status); err != nil {
			panic(err)
		}

		//Se possui filtro de nome entao verifica se o funcionario retornado está naquela massa de dados.
		if len(Nome) > 0 {
			possuiFiltro = true
			if strings.Contains(funcionario.Nome, Nome) {
				funcionarios = append(funcionarios, funcionario)
			}
		} else if len(Cpf) > 0 {
			possuiFiltro = true
			if strings.Contains(funcionario.Cpf, Cpf) {
				funcionarios = append(funcionarios, funcionario)
			}
		}
		if possuiFiltro == false {
			funcionarios = append(funcionarios, funcionario)
		}
	}

	if err := json.NewEncoder(w).Encode(funcionarios); err != nil {
		panic(err)
	}

}
