package model

import (
	"database/sql"
	"strings"
)

//Funcionario struct
type Funcionario struct {
	Id         int64   `json:"Id"`
	DataCad    string  `json:"DataCad"`
	Cargo      string  `json:"Cargo"`
	Cpf        string  `json:"Cpf"`
	Nome       string  `json:"Nome"`
	UfNasc     string  `json:"UfNasc"`
	Salario    float64 `json:"Salario"`
	Status     string  `json:"Status"`
	SalarioMin float64
	SalarioMax float64
}

//POST Funcionario
func (f *Funcionario) InsertFuncionario(db *sql.DB) error {

	statement, err := db.Prepare(`INSERT INTO funcionarios (DataCad, Cargo, Cpf, Nome, UfNasc, Salario, Status)
								values
								(?, ?, ?, ?, ?, ?, ?)`)
	if err != nil {
		return err
	}

	res, err := statement.Exec(f.DataCad, f.Cargo, f.Cpf, f.Nome, f.UfNasc, f.Salario, f.Status)
	if err != nil {
		return err
	}
	Id, _ := res.LastInsertId()
	f.Id = Id
	return nil
}

//PUT Funcionario
func (f *Funcionario) UpdateFuncionario(db *sql.DB) error {
	statement, err := db.Prepare(`UPDATE funcionarios set DataCad = ?, Cargo = ?, Cpf = ?, Nome = ?, UfNasc = ?, Salario = ?, Status = ? WHERE Id = ?`)

	if err != nil {
		return err
	}

	_, err = statement.Exec(f.DataCad, f.Cargo, f.Cpf, f.Nome, f.UfNasc, f.Salario, f.Status, f.Id)

	return err
}

//GET Funcionario
func (f *Funcionario) GetFuncionario(db *sql.DB) error {
	err := db.QueryRow(`SELECT Id, DataCad, Cargo, Cpf, Nome, UfNasc, Salario, Status
					from funcionarios
					WHERE Id =  ?`, f.Id).Scan(&f.Id, &f.DataCad, &f.Cargo, &f.Cpf, &f.Nome, &f.UfNasc, &f.Salario, &f.Status)
	if err != nil {
		return err
	}

	return err
}

//GET Funcionarios
func (f *Funcionario) GetFuncionarios(db *sql.DB) ([]Funcionario, error) {
	var values []interface{}
	var where []string

	if f.DataCad != "" {
		where = append(where, "DataCad = ?")
		values = append(values, f.DataCad)
	}

	if f.Nome != "" {
		where = append(where, "Nome = ?")
		values = append(values, f.Nome)
	}

	if f.Cpf != "" {
		where = append(where, "Cpf = ?")
		values = append(values, f.Cpf)
	}

	if f.Cargo != "" {
		where = append(where, "Cargo = ?")
		values = append(values, f.Cargo)
	}

	if f.SalarioMin != 0 && f.SalarioMax == 0 {
		where = append(where, "Salario >= ?")
		values = append(values, f.SalarioMin)
	} else if f.SalarioMin == 0 && f.SalarioMax != 0 {
		where = append(where, "Salario <= ?")
		values = append(values, f.SalarioMax)
	} else if f.SalarioMin != 0 && f.SalarioMax != 0 {
		where = append(where, "Salario BETWEEN ? AND ?")
		values = append(values, f.SalarioMin, f.SalarioMax)
	}

	q := `SELECT Id, DataCad, Cargo, Cpf, Nome, UfNasc, Salario, Status
			FROM funcionarios
			WHERE True`
	if len(where) != 0 {
		q = q + " AND " + strings.Join(where, " AND ")
	}

	rows, err := db.Query(q, values...)
	if err != nil {
		return nil, err
	}

	funcionarios := []Funcionario{}
	defer rows.Close()
	for rows.Next() {
		var funcionario Funcionario
		if err = rows.Scan(&funcionario.Id, &funcionario.DataCad, &funcionario.Cargo, &funcionario.Cpf, &funcionario.Nome, &funcionario.UfNasc, &funcionario.Salario, &funcionario.Status); err != nil {
			return nil, err
		}
		funcionarios = append(funcionarios, funcionario)
	}
	return funcionarios, nil
}

//DELETE Funcionario
func (f *Funcionario) DeleteFuncionario(db *sql.DB) error {
	statement, err := db.Prepare(`DELETE from funcionarios WHERE Cpf = ?`)

	if err != nil {
		return err
	}

	_, err = statement.Exec(f.Cpf)

	return err
}
